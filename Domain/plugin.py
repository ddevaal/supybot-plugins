###
# Copyright (c) 2011, Dennis de Vaal
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

# About pywhois limitations:
#
# pywhois likes to return empty WhoisEntry objects when none of its regexes match 
# either because of a connection failure, a limit on the number of lookups 
# was imposed by the whois server and this limit was exceeded or because the domain 
# whois directory service is not yet supported (e.g. .nl) and a basic WhoisEntry 
# object is used as a fallback, unfortunately pywhois does not provide error handling 
# for these type of situations.
import pywhois

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks

class Domain(callbacks.Plugin):
    """Add the help for "@plugin help Domain" here
    This should describe *how* to use this plugin."""

    def whois(self, irc, msg, args, domains, raw):
        """<domain1, [domain2, ...]> [<raw>]
        Add True at the end of this command to obtain raw whois-server output.
        Returns WHOIS information on the registration of <domain1..n>.
        """

        results = []
        for domain in domains:
            try:
                result = pywhois.whois(domain)
            except pywhois.parser.PywhoisError, e:
                irc.error(str(e), Raise=True)
            else:
                [getattr(result, key) for key in result._regex.keys()] # pywhois overloads attribute lookups so we need to call each one to let pywhois initialize them
                s = "Domain: {domain}, Created On: {creation_date}, Expires On: {expiration_date}, Last Updated On: {updated_date}, Registrar: {registrar}".format(**result.__dict__) # unwrapping dictionary to keyword args
                if raw: s += ", Raw Output: {text}".format(text=result.__dict__['text'])              
                results.append(s) 
        return irc.reply(', '.join(results))
    whois = wrap(whois, [commalist('something'), optional('boolean')])
Class = Domain


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
